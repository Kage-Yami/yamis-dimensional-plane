# Changelog <!-- omit in toc -->

- [3.0.0-beta1](#300-beta1)
- [2.4.0](#240)
- [2.3.0](#230)
- [2.2.0](#220)
- [2.1.0](#210)
- [2.0.0](#200)
- [1.0.1](#101)
- [1.0.0](#100)

## 3.0.0-beta1

**Updated Forge ⇢ 3.1.24**

**Removed:**

- Upstream

**Added:**

- Basic Nether Ores [Forge/Fabric]
- Chunk-Pregenerator
- Fruitful
- [FORGE] Outvoted
- Regrowth
- Snow! Real Magic! ⛄
- Snow Under Trees
- Stoneholm, Underground Villages (Forge)
- TerraForged
- The Twilight Forest
- Valhelsia Structures

**Configuration:**

- Default world-gen type is now TerraForged instead of Quark Realistic

**Updated:**

- Clumps ⇢ `6.0.0.23`
- CraftTweaker ⇢ `7.1.0.296`
- Performant ⇢ `3.58`
- Tetra ⇢ `3.11.1`

## 2.4.0

**Updated Forge ⇢ 36.1.23**

**Added:**

- CC: Tweaked
- Entangled
- ItemPhysic Full
- Light Overlay (Rift/Forge/Fabric)
- Modern UI
- NoMoWanderer
- Trample Stopper

**Configuration:**

- Disable crafting recipes for Warp Plates and Sharestones (from new version)
- Add mixing crafting recipe for Crushed Brass from Copper Dust
- Add Create-based recipes for Mekanism metals:
    - Enriched Iron
    - Netherite
    - Bronze
    - Steel
    - Refined Obsidian Dust

**Updated:**

- Advanced Chimneys ⇢ `6.0.9.0`
- Architectury API (Forge) ⇢ `1.15.13`
- Autumnity ⇢ `2.1.1`
- Better Advancements ⇢ `0.1.0.108`
- Bookshelf ⇢ `10.0.8`
- Botania ⇢ `416`
- Clumps ⇢ `6.0.0.22`
- Collective ⇢ `2.26`
- Controlling ⇢ `7.0.0.16`
- CraftTweaker ⇢ `7.1.0.294`
- Create Crafts & Additions ⇢ `20210517a`
- CreateTweaker ⇢ `1.0.0.12`
- Cyclops Core ⇢ `1.11.6`
- Dynamic Surroundings ⇢ `4.0.4.2`
- Enchantment Descriptions ⇢ `7.0.8`
- Eyes in the Darkness ⇢ `1.0.2`
- Flux Networks ⇢ `6.1.7.12`
- Inventory HUD+ ⇢ `3.3.0`
- Just Enough Items (JEI) ⇢ `7.7.0.99`
- Just Enough Resources (JER) ⇢ `0.12.1.114`
- Large Ore Deposits ⇢ `4.1.5.1`
- Mantle ⇢ `1.6.97`
- mGui ⇢ `3.2.0`
- Patchouli ⇢ `51`
- Performant ⇢ `3.56`
- Placebo ⇢ `4.4.5`
- Platforms ⇢ `1.7.11`
- Pollution of the Realms ⇢ `4.0.3.0`
- Psi ⇢ `64`
- Quark ⇢ `r2.4-311`
- Tetra ⇢ `3.10.0`
- The One Probe ⇢ `3.1.4`
- Waystones ⇢ `7.5.1`
- YUNG's Better Caves (Forge) ⇢ `1.1.2`

## 2.3.0

**Removed:**

- Custom login messages

**Added:**

- BlueMap

**Updated:**

- Advanced Chimneys ⇢ `1.16.4-6.0.8.0-build.0108`
- Architectury API (Forge) ⇢ `1.12.145`
- Clumps ⇢ `6.0.0.21`
- CraftTweaker ⇢ `1.16.5-7.1.0.216`
- Create ⇢ `mc1.16.5_v0.3.1c`
- Create Crafts & Additions ⇢ `1.16.5-20210417a`
- CreateTweaker ⇢ `1.0.0.11`
- Curios API (Forge) ⇢ `1.16.5-4.0.5.1`
- Enchantment Descriptions ⇢ `1.16.5-7.0.7`
- Enhanced Celestials - Blood Moons & Harvest Moons ⇢ `1.0.4-1.16.4`
- JEITweaker ⇢ `1.16.5-1.0.1.15`
- Just Enough Items (JEI) ⇢ `1.16.5-7.6.4.88`
- Lootr ⇢ `1.16.4-0.0.5.17`
- Mantle ⇢ `1.16.5-1.6.92`
- Pollution of the Realms ⇢ `1.16.4-4.0.2.0-build.0108`

## 2.2.0

**Removed:**

- DynmapForge

**Added:**

- CreateTweaker

**Configuration:**

- Added recipes to process crushed aluminum ore into aluminum ingots/nuggets (smelting/washing)

## 2.1.0

_Note: existing worlds will load without issues, but all items from Scaling Health will be deleted._

**Removed:**

- Scaling Health

**Added:**

- Custom login messages

**Updated:**

- CraftTweaker ⇢ `1.16.5-7.1.0.188`
- Cyclops Core ⇢ `1.16.5-1.11.5`
- Enhanced Celestials - Blood Moons & Harvest Moons ⇢ `1.0.3-1.16.4`

## 2.0.0

_Note: existing worlds will load without issues, but all Realistic Torches will be deleted._

**Removed:**

- Realistic Torches
- Total Darkness

**Updated:**

- Advanced Chimneys ⇢ `1.16.4-6.0.7.0-build.0094`
- CraftTweaker ⇢ `1.16.5-7.1.0.186`
- Create ⇢ `1.16.5_v0.3.1a`
- Create Crafts & Additions ⇢ `1.16.5-20210404a`
- Large Ore Deposits ⇢ `1.16.4-4.1.5.0-build.0096`
- Ore Excavation ⇢ `1.8.157`
- Quark ⇢ `r2.4-310`
- Tetranomicon ⇢ `1.2`
- "To the Bat Poles!" ⇢ `1.16.4-5.0.4.0-build.0095`
- YUNG's API (Forge) ⇢ `1.16.4-Forge-6`

## 1.0.1

**Removed:**

- SQLConnectors

## 1.0.0

_Initial release._
