// Tags for Mekanism infusion materials
<tag:items:yami:infusion/coal>.add(<tag:items:minecraft:coals>);
<tag:items:yami:infusion/coal>.add(<tag:items:forge:dusts/coal>);
<tag:items:yami:infusion/coal>.add(<tag:items:forge:dusts/charcoal>);
<tag:items:yami:infusion/coal>.add(<tag:items:forge:storage_blocks/coal>);
<tag:items:yami:infusion/coal>.add(<tag:items:forge:storage_blocks/charcoal>);
<tag:items:yami:infusion/coal>.add(<tag:items:mekanism:enriched/carbon>);

<tag:items:yami:infusion/tin>.add(<tag:items:forge:dusts/tin>);
<tag:items:yami:infusion/tin>.add(<tag:items:mekanism:enriched/tin>);

<tag:items:yami:infusion/diamond>.add(<tag:items:forge:dusts/diamond>);
<tag:items:yami:infusion/diamond>.add(<tag:items:mekanism:enriched/diamond>);

// Add recipe for Enriched Iron
<recipetype:create:mixing>.addRecipe("mix_enriched_iron", "heated", <item:mekanism:enriched_iron>, [
    <item:create:crushed_iron_ore> | <tag:items:forge:ingots/iron> | <tag:items:forge:dusts/iron>,
    <tag:items:yami:infusion/coal>
]);

// Add recipes for Netherite
<recipetype:create:crushing>.addRecipe(
    "crush_dirty_netherite_scrap", [<item:mekanism:dirty_netherite_scrap> * 2], <tag:items:forge:ores/netherite_scrap>
);
<recipetype:create:splashing>.addRecipe(
    "wash_netherite_dust",
    [
        <item:charm:netherite_nugget> * 10,
        <item:charm:netherite_nugget> % 50,
        <item:charm:netherite_nugget> % 50,
        <item:charm:netherite_nugget> % 50,
        <item:charm:netherite_nugget> % 50,
        <item:charm:netherite_nugget> % 50
    ],
    <tag:items:forge:dusts/netherite>
);

// Add recipes for Bronze
<recipetype:create:mixing>.addRecipe("mix_bronze_dust", "heated", <item:mekanism:dust_bronze> * 4, [
        (<item:create:crushed_copper_ore> | <tag:items:forge:dusts/copper>) * 3,
        <tag:items:yami:infusion/tin>
]);
<recipetype:create:splashing>.addRecipe(
    "wash_bronze_dust",
    [
        <item:mekanism:nugget_bronze> * 10,
        <item:mekanism:nugget_bronze> % 50,
        <item:mekanism:nugget_bronze> % 50,
        <item:mekanism:nugget_bronze> % 50,
        <item:mekanism:nugget_bronze> % 50,
        <item:mekanism:nugget_bronze> % 50
    ],
    <tag:items:forge:dusts/bronze>
);

// Add recipes for Steel
<recipetype:create:mixing>.addRecipe("mix_steel_dust", "heated", <item:mekanism:dust_steel>, [
    <item:mekanism:enriched_iron>,
    <tag:items:yami:infusion/coal>
]);
<recipetype:create:splashing>.addRecipe(
    "wash_steel_dust",
    [
        <item:mekanism:nugget_steel> * 10,
        <item:mekanism:nugget_steel> % 50,
        <item:mekanism:nugget_steel> % 50,
        <item:mekanism:nugget_steel> % 50,
        <item:mekanism:nugget_steel> % 50,
        <item:mekanism:nugget_steel> % 50
    ],
    <tag:items:forge:dusts/steel>
);

// Add recipe for Refined Obsidian Dust
<recipetype:create:mixing>.addRecipe("mix_refined_obsidian", "heated", <item:mekanism:dust_refined_obsidian>, [
    <tag:items:forge:dusts/obsidian>,
    <tag:items:yami:infusion/diamond>
]);
